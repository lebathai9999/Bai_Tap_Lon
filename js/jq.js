//Scroll Menu
$(document).ready(function(e) {
    $(this).scroll(function(){
		if($(this).scrollTop()>0){
			$('header').addClass('sec-header-move');
			$('#logo-hublot').css('fill','white');
			$('ul.menu > li > a').removeClass('menu-move-new').addClass('menu-move');
			$('sec-search-cart .cart div').removeClass('cart-white').addClass('cart-black');
		}else{
			$('header').removeClass('sec-header-move');
			$('#logo-hublot').css('fill','black');
			$('ul.menu > li > a').removeClass('menu-move').addClass('menu-move-new');
			$('sec-search-cart .cart div').removeClass('cart-black').addClass('cart-white');
		}
	});
});


//Create Tab for product
$(document).ready(function() {
    // ẩn tất cả các thẻ div với class="tab_content".
    $(".tab_content").hide();
    // Mặc định nội dung thẻ tab đầu tiên sẽ được hiển thị
    $(".tab_content:first").show();
 
    $("ul.tabs  li").click(function() {
        // gỡ bỏ class="active" cho tất cả các thẻ
        $("ul.tabs  li").removeClass("active");
        // chèn class="active" vào phần tử </li> vừa được click
        $(this).addClass("active");
        // ẩn tất cả thẻ với class="tab_content"
        $(".tab_content").hide();
        //Hiển thị nội dung thẻ tab được click với hiệu ứng Fade In
        var activeTab = $(this).attr("rel");
        $("#"+activeTab).fadeIn();
    });
});

//slide news
$(document).ready(function() {
	var owl = $('.owl-news');
	owl.owlCarousel({
		margin: 20,
		loop:true,
		responsive: {
			0: {items: 1},
			600: {items: 3},
			1000: {items: 3}
			}
	})
})
				
				
//Opinion Client
$(document).ready(function() {
      var owl_op = $('.owl-opinion');
      owl_op.owlCarousel({
           margin: 20,
           loop:true,
           responsive: {
              0: {items: 1},
			  600: {items: 1},
			  1000: {items: 1}
           }
       })
})


//Product Equal
$(document).ready(function() {
      var owl_re = $('.owl-relate');
      owl_re.owlCarousel({
           margin: 20,
           loop:true,
           responsive: {
              0: {items: 1},
			  600: {items: 3},
			  1000: {items: 3}
           }
       })
})


//up to top
jQuery(document).ready(function() {
	var offset = 220;
	var duration = 500;
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > offset) {
			jQuery(".lendt").fadeIn(duration);
		} else {
			jQuery('.lendt').fadeOut(duration);
		}
		});
	 
	jQuery('.lendt').click(function(event) {
		event.preventDefault();
		jQuery('html, body').animate({scrollTop: 0}, duration);
		return false;
	})
});

//JQuery About us
$(document).ready(function(){
      $(".main").onepage_scroll({
        sectionContainer: "section",
        responsiveFallback: 600,
        loop: true
      });
});

function LoadForm(){
	setTimeout(function(){
		$(".page1 .sec-craft p").addClass("js-animate-title").css('opacity','1');
		$(".page1 .sec-craft h2").addClass("js-animate-title").css('opacity','1');
	},120);
}

$(document).ready(function(e) {
	$(".sec-button-page2").click(function(){
		$(".detail-about-page2").css('transform','scaleY(1)');	
	});
	$(".sec-button-page3").click(function(){
		$(".detail-about-page3").css('transform','scaleY(1)');	
	});
	$(".sec-button-page4").click(function(){
		$(".detail-about-page4").css('transform','scaleY(1)');	
	});
	$(".sec-button-page5").click(function(){
		$(".detail-about-page5").css('transform','scaleY(1)');	
	});
	$(".back").click(function(){
		$(".detail-about-page2").css('transform','scaleY(0)');
		$(".detail-about-page3").css('transform','scaleY(0)');
		$(".detail-about-page4").css('transform','scaleY(0)');
		$(".detail-about-page5").css('transform','scaleY(0)');	
	});      
});	
