<?php session_start();include("connect.php")?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="../css/css.css"/>
<link rel="stylesheet" type="text/css" href="../css/font-fa/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="../css/onepage-scroll.css"/>
<link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css"/>
<script type="text/javascript" src="../js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.onepage-scroll.js"></script>
<script type="text/javascript" src="../js/jq.js"></script>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
</head>

<body>
<?php
	if(!isset($_SESSION["admin"])){
		include("logadmin.php");	
	}else{
		include("control.php");	
	}
?>
</body>
</html>